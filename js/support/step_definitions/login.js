(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
"use strict";

given("the user has the correct credentials", function () {
  cy.visit('https://sprinkle-burn.glitch.me/'); 
});
when("the user enters username", function () {
  //get input field and enter value
  cy.get('input[name=email]').type('test@drugdev.com'); 
});
then("the user enters password", function () {
  //get password field and enter value
  cy.get('input[name=password]').type('supers3cret'); 
});
then("clicks Login", function () {
  //click login
  cy.get('button[aria-label=login]').click(); 
});
then("the user is presented with a welcome message", function () {
  //get div containing welcome message and compare it to the expected result
  cy.get('.content').should(function ($content) {
    expect($content).to.contain('Welcome Dr I Test');
  }); //throw new Error("Not implemented");
});
given("the user has the incorrect credentials", function () {
  cy.visit('https://sprinkle-burn.glitch.me/'); 
});
/*
decided that the username would be the "incorrect" value.
Had to change the name of the test in the feature spec so it
didnt call the original one
*/

when("the user enters incorrect username", function () {
  cy.get('input[name=email]').type('hireme@please.com'); 
});
/*
no need to have another func to enter password or click func, this already exists above
*/

then("the user is presented with a error message", function () {
  //get div containing welcome message and compare it to the expected result
  cy.get('div[id=login-error-box]').should(function ($content) {
    expect($content).to.contain('Credentials are incorrect');
  }); 
});

},{}]},{},[1]);
