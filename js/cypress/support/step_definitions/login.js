/*
  might be a good idea to pass variables through cucumber containing the
  usernames and passwords required
*/


given("the user has the correct credentials", () => {
  cy.visit('https://sprinkle-burn.glitch.me/');
  
  
});

when("the user enters username", () => {
  //get input field and enter value
  cy.get('input[name=email]').type('test@drugdev.com');
  
});

then("the user enters password", () => {
  //get password field and enter value
  cy.get('input[name=password]').type('supers3cret');
  
});

then("clicks Login", () => {
  //click login
  cy.get('button[aria-label=login]').click();
  
});

then("the user is presented with a welcome message", () => {
  //get div containing welcome message and compare it to the expected result
  cy.get('.content').should(($content)=>{
    expect($content).to.contain('Welcome Dr I Test')
  })
  
});

given("the user has the incorrect credentials", () => {
  cy.visit('https://sprinkle-burn.glitch.me/');
  

});

/*
decided that the username would be the "incorrect" value.
Had to change the name of the test in the feature spec so it
didnt call the original one
*/
when("the user enters incorrect username", () => {
  cy.get('input[name=email]').type('hireme@please.com');
  
});

/*
no need to have another func to enter password or click func
*/
then("the user is presented with a error message", () => {
  //get div containing welcome message and compare it to the expected result
  cy.get('div[id=login-error-box]').should(($content)=>{
      expect($content).to.contain('Credentials are incorrect')
  })

});
