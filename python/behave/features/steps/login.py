from behave import *
import time

username = None
password = None

@given(u'the user has the correct credentials')
def step_impl(context):
    context.browser.get('https://sprinkle-burn.glitch.me/')
    global username
    global password
    username = "test@drugdev.com"
    password = "supers3cret"
    

@when(u'the user enters username')
def step_impl(context):
    emailField = context.browser.find_element_by_name("email")
    emailField.send_keys(username)
    


@when(u'the user enters password')
def step_impl(context):
    pwField = context.browser.find_element_by_name("password")
    pwField.send_keys(password)
    

@when(u'clicks Login')
def step_impl(context):
    loginButton = context.browser.find_element_by_tag_name("button")
    loginButton.click()
    time.sleep(1)
    

@then(u'the user is presented with a welcome message')
def step_impl(context):
    welcomeContainer = context.browser.find_element_by_tag_name("article")
    welcomeText = welcomeContainer.text
    assert welcomeText == "Welcome Dr I Test"
    

@given(u'the user has the incorrect credentials')
def step_impl(context):
    context.browser.get('https://sprinkle-burn.glitch.me/')
    global username
    global password
    username = "test@drugdev.com"
    password = "pythonisalrightiguess"
    

@then(u'the user is presented with a error message')
def step_impl(context):
    errorContainer = context.browser.find_element_by_id("login-error-box")
    errorText = errorContainer.text
    assert errorText == "Credentials are incorrect"
    